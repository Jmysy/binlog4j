package com.gitee.Jmysy.binlog4j.springboot.starter.example.domain;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BinlogPosition {

    private LocalDateTime createTime;

    private String text;
}
