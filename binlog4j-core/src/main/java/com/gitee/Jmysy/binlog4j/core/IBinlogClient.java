package com.gitee.Jmysy.binlog4j.core;

/**
 * Binlog 客户端接口
 *
 * @author 就眠儀式
 *  * */
public interface IBinlogClient {

    /**
     * 开始连接
     * */
    void connect();

    /**
     * 注册 binlog event 处理器
     *
     * @param eventHandler 事件处理器
     * */
    void registerEventHandler(IBinlogEventHandler eventHandler);

    /**
     * 注册 binlog event 处理器
     *
     * @param handlerKey 具名 Key
     * @param eventHandler 事件处理器
     * */
    void registerEventHandler(String handlerKey, IBinlogEventHandler eventHandler);

    /**
     * 注销 binlog event 处理器
     *
     * @param handlerKey 具名 Key
     * */
    void unregisterEventHandler(String handlerKey);

    /**
     * 断开连接
     * */
    void disconnect();

}
