package com.gitee.Jmysy.binlog4j.core.dispatcher;

import com.gitee.Jmysy.binlog4j.core.BinlogClientConfig;
import com.gitee.Jmysy.binlog4j.core.BinlogEventHandlerInvoker;
import com.gitee.Jmysy.binlog4j.core.position.BinlogPosition;
import com.gitee.Jmysy.binlog4j.core.position.BinlogPositionHandler;
import com.github.shyiko.mysql.binlog.BinaryLogClient;
import com.github.shyiko.mysql.binlog.event.*;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BinlogEventDispatcher implements BinaryLogClient.EventListener {
    private final Map<Long, TableMapEventData> tableMap = new HashMap<>();

    private final Map<String,BinlogEventHandlerInvoker> eventHandlerMap;

    private final BinlogClientConfig clientConfig;

    private final BinlogPositionHandler binlogPositionHandler;

    public BinlogEventDispatcher(BinlogClientConfig clientConfig, BinlogPositionHandler positionHandler, Map<String ,BinlogEventHandlerInvoker> eventHandlerMap) {
        this.clientConfig = clientConfig;
        this.eventHandlerMap = eventHandlerMap;
        this.binlogPositionHandler = positionHandler;
    }

    @Override
    public void onEvent(Event event) {
        EventHeaderV4 headerV4 = event.getHeader();
        EventType eventType = headerV4.getEventType();
        if (eventType == EventType.TABLE_MAP) {
            TableMapEventData eventData = event.getData();
            tableMap.put(eventData.getTableId(), eventData);
        } else {
            if (EventType.isRowMutation(eventType)) {
                RowMutationEventData rowMutationEventData = new RowMutationEventData(event.getData());
                TableMapEventData tableMapEventData = tableMap.get(rowMutationEventData.getTableId());
                if (tableMapEventData != null) {
                    String database = tableMapEventData.getDatabase();
                    String table = tableMapEventData.getTable();
                    this.eventHandlerMap.forEach((handlerKey, eventHandler) -> {
                        if (EventType.isUpdate(eventType)) {
                            eventHandler.invokeUpdate(database, table, rowMutationEventData.getUpdateRows());
                            return;
                        }
                        if (EventType.isDelete(eventType)) {
                            eventHandler.invokeDelete(database, table, rowMutationEventData.getDeleteRows());
                            return;
                        }
                        if (EventType.isWrite(eventType)) {
                            eventHandler.invokeInsert(database, table, rowMutationEventData.getInsertRows());
                        }
                    });
                }
            }
        }
        if (clientConfig.getPersistence()) {
            if (binlogPositionHandler != null) {
                if(eventType != EventType.FORMAT_DESCRIPTION) {
                    BinlogPosition binlogPosition = new BinlogPosition();
                    if (EventType.ROTATE == eventType) {
                        binlogPosition.setServerId(clientConfig.getServerId());
                        binlogPosition.setFilename(((RotateEventData) event.getData()).getBinlogFilename());
                        binlogPosition.setPosition(((RotateEventData) event.getData()).getBinlogPosition());
                    } else {
                        binlogPosition = binlogPositionHandler.loadPosition(clientConfig.getServerId());
                        if (binlogPosition != null) {
                            binlogPosition.setPosition(headerV4.getNextPosition());
                        }
                    }
                    binlogPositionHandler.savePosition(binlogPosition);
                }
            }
        }
    }

    @Data
    public static class RowMutationEventData {

        private long tableId;

        private List<Serializable[]> insertRows;

        private List<Serializable[]> deleteRows;

        private List<Map.Entry<Serializable[], Serializable[]>> updateRows;

        public RowMutationEventData(EventData eventData) {

            if (eventData instanceof UpdateRowsEventData) {
                UpdateRowsEventData updateRowsEventData = (UpdateRowsEventData) eventData;
                this.tableId = updateRowsEventData.getTableId();
                this.updateRows = updateRowsEventData.getRows();
                return;
            }

            if (eventData instanceof WriteRowsEventData) {
                WriteRowsEventData writeRowsEventData = (WriteRowsEventData) eventData;
                this.tableId = writeRowsEventData.getTableId();
                this.insertRows = writeRowsEventData.getRows();
                return;
            }

            if (eventData instanceof DeleteRowsEventData) {
                DeleteRowsEventData deleteRowsEventData = (DeleteRowsEventData) eventData;
                this.tableId = deleteRowsEventData.getTableId();
                this.deleteRows = deleteRowsEventData.getRows();
            }
        }
    }
}
