package com.gitee.Jmysy.binlog4j.core.position;

public interface BinlogPositionHandler {

    BinlogPosition loadPosition(Long serverId);

    void savePosition(BinlogPosition position);
}
