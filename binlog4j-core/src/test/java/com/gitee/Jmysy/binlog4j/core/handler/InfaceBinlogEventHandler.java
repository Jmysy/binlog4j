package com.gitee.Jmysy.binlog4j.core.handler;

import com.gitee.Jmysy.binlog4j.core.BinlogEvent;
import com.gitee.Jmysy.binlog4j.core.IBinlogEventHandler;
import com.gitee.Jmysy.binlog4j.core.domain.Binlog4j;

public class InfaceBinlogEventHandler implements IBinlogEventHandler<Binlog4j> {

    @Override
    public void onInsert(BinlogEvent<Binlog4j> event) {
        System.out.println("插入事件:" + event.getData());
    }

    @Override
    public void onUpdate(BinlogEvent<Binlog4j> event) {
        System.out.println("修改事件:" + event.getData());
    }

    @Override
    public void onDelete(BinlogEvent<Binlog4j> event) {
        System.out.println("删除事件:" + event.getData());
    }

    @Override
    public boolean isHandle(String database, String table) {
        return true;
    }

}
